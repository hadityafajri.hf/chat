var fs = require('fs')
var fetch = require('node-fetch')
const path = require('path');

var server = require('./server.js')
var cache = require('./usersInRoomsCache.js')

var ethSigUtil = require('eth-sig-util')
var HttpProvider = require('ethjs-provider-http');
var Eth = require('ethjs');

var config = JSON.parse(fs.readFileSync(path.join(__dirname, '../config.json')));
var network = config.networks.rinkeby
var eth = new Eth(new HttpProvider(network.provider));
var abi = config.abi

var db = require('./db');
var dbCallback = function(err, result) {
    if (err) throw err;
};

module.exports = {
    ping,
    login,
    logout,
    message,
    sendEthTransaction,
    sendTransaction,
    getOnline,
    whisper
}

function ping () {}

// validates signed token (recent blockhash + site string) to authenticate user
// validates user by calling getPermissions on room contract
// upon success, sends saved messages to user
async function login (args) {
    var socket = this;
    var userAddress = args.from;
    var roomAddress = args.room;
    var loginToken;

    if (cache.isUserInRoom(userAddress, roomAddress, socket.key)) {
        return server.send({cmd: "message", type: "alert", text:"You're already signed into this chatroom from another session", from: 'SERVER'}, socket);
    }

    try {
        loginToken = JSON.parse(Eth.toAscii(args.data))
    } catch(e){
        return console.log(e)
    }

    let signedBlock = await eth.getBlockByHash(loginToken.blockHash, false);
    if (signedBlock == null) {
        return server.send({cmd:"message", type: "alert", text:"Invalid blockhash, you may be on the wrong network", from:'SERVER'}, socket)
    }

    let msgParams = {data: args.data, sig: args.sig};
    let recoveredSig = ethSigUtil.recoverPersonalSignature(msgParams);
    let latestBlock = await eth.blockNumber();

    // VALIDATE LOGIN TOKEN
    if (latestBlock.toNumber() - signedBlock.number.toNumber() > 5 ||
        recoveredSig !== userAddress || loginToken.site !== 'chat') {
        return server.send({cmd:"message", type: "alert", text:"Invalid or out of data login token", from:'SERVER'}, socket)
    }

    //Decode the address and params from the room string
    let roomParams = args.room.split('/')
    let contractAddress = roomParams[0]
    let roomABI = abi

    //CHECK FOR SUPPLIED ABI
    let hash = await eth.contract(roomABI).at(contractAddress).metadata();
    if (hash[0].slice(0,2) === 'Qm' ) {
        let metadata;
        let fetchResult = (await db.getDataFromIpfsHash(hash[0]));
        if (fetchResult !== null && fetchResult.hasOwnProperty(metadata)) {
            metadata = fetchResult.metadata;
        }

        // case: data is not yet in db => retrieve it from ipfs and save it to db
        if ( !metadata ) {
            metadata = await (await fetch("https://ipfs.infura.io:5001/api/v0/cat?arg=" + hash[0])).json();
            await db.saveIpfsHash(hash[0], metadata);
        }

        if ("abi" in metadata) {
            roomABI = metadata.abi;
        }
    }

    //CHECK USER PERMISSIONS
    let permissions;
    let permissionsArguments = [args.from].concat(roomParams.slice(1));
    let roomInstance = await eth.contract(roomABI).at(contractAddress);
    try {
        permissions = await roomInstance.getPermissions(...permissionsArguments)
    } catch(e) {
        return server.send({cmd:'message', type:'alert', text:"This is not a valid address!", from: "SERVER"}, socket)
    }

    // Case: invalid contract address
    if (permissions[0] === '0x') {
        return server.send({cmd:'message', type:'alert', text:"This address is not a valid contract!", from: "SERVER"}, socket)
    }

    // Case: Success: add user to room; send pastMessages; notify room
    else if (permissions[0].substr(permissions[0].length -1) === '1' ) {

        cache.saveRoom(roomAddress);
        cache.saveUser(userAddress, socket.key, roomAddress);

        let pastMessages = await (await db.getAllMessages(roomAddress)).toArray()
        pastMessages.forEach(function(item) {
            server.send({
                cmd: item.cmd,
                text: item.text,
                from: item.user_id,
                time: item.time
            }, socket);
        })

        server.broadcast({
            cmd: 'message',
            type: 'notification',
            text: userAddress + " joined",
            from: "SERVER"
        }, roomAddress);

        server.send({cmd:'loggedIn'}, socket)

        module.exports.getOnline.call(socket, {room: roomAddress, from: userAddress});
    }
    else {
        // Case: user is not able to enter (contract) chat room
        server.send({cmd:'message', type:'alert', text: "You're not authenticated to use this chatroom", from: "SERVER"}, socket);
    }
}

function logout() {
    var socket = this;
    var msg;
    let cachedValues = cache.getInfoFromSocket(socket.key);
    let { user, room } = cachedValues;

    if (user && room) {
        // remove user-socket pairing from one cache
        if (cache.removeUser(user, socket.key, room)) {
            msg = user + " left";
            server.broadcast({cmd:'message', type:'notification', text: msg, from: "SERVER"}, room);
            server.send({cmd:'message', type:'alert', text: "YOU LOGGED OUT!", from: "SERVER"}, socket);
        }
    }
}

function message (args) {
    var text = String(args.text)
    var socket = this;
    var userAddress, roomAddress, data;

    // use socketKey to get user & room info
    let { user, room } = cache.getInfoFromSocket(socket.key);

    // broadcast message to room, save message to db
    if (user && room) {
        data = {cmd: 'message',type:'message', text: text, from: user, time: Date.now()};
        server.broadcast(data, room);
        db.saveMessage(data, room, dbCallback);
    }
}

function whisper (args) {
    var text = String(args.text)
    var socket = this;
    var userAddress, recipientAddress, roomAddress, data;

    // use socketKey to get user & room info
    let { user, room } = cache.getInfoFromSocket(socket.key);
    // use userAddress to get lookup if recipient is online
    recipientAddress = args.to
    let recipientSocket = cache.getAllUsersInRoom(room)[recipientAddress]
    if (user && room) {
        if (recipientSocket) {
            server.send({cmd:'message', type:'whisper',text:text, from:user, time:Date.now()}, recipientSocket)
            server.send({cmd:'message', type:'whisper',text:text, from:user, time:Date.now()}, socket)
        } else {
            //case recipient is not online
            server.send({cmd:'message', type:'notification', text: "The recipient is not online. Nobody heard you.", from: "SERVER"}, socket);
        }
    }
}

function sendEthTransaction (args) {
    var socket = this;
    var receipt = args.receipt
    var recipient = args.recipient

    eth.getTransactionByHash(receipt).then(function(txData) {
        if (txData != null && recipient == txData.to ){
            server.sendToAddress({cmd:'ethTransaction', receipt: receipt}, recipient, socket);
        }
    }).catch(function(e){
        console.log(e)
    })
}

function sendTransaction (args) {
    var receipt = args.receipt
    var socket = this;
    var roomAddress;

    // get room info from socket key
    const { user, room } = cache.getInfoFromSocket(socket.key);

    eth.getTransactionByHash(receipt)
    .then(function(txData){
        if(txData != null){
            server.broadcast({cmd:'transaction', receipt: receipt, from: txData.from, command: args.command}, room)
        }
    })
    .catch(function(err){
        console.log('ERROR: ', err)
    });
}

// get list of users in room and notify new user
function getOnline (args) {
    var socket = this;
    var roomAddress = args.room;
    var userAddress = args.from;
    var filteredUsers;
    var message;

    let usersSocketsObj = cache.getAllUsersInRoom(roomAddress);
    let usersArr = Object.keys(usersSocketsObj);

    // filter the user out of users array
    filteredUsers = usersArr.filter(user => user !== userAddress);

    if (filteredUsers.length === 0) {
        message = "online: no one else :("
    } else {
        message = "online: " + filteredUsers.join(', ');
    }

    server.send({
        cmd: 'message',
        type: 'notification',
        text: message,
        from: "SERVER"
    }, socket);
}

