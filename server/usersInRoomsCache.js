var usersInRoomsCache = function() {

    var usersInRoom = {}; // {room_id: {user_id: socketKey}} (user_id is an address)
    var socketsUsers = {}; // {socketKey: user_id}

    return {
        saveRoom: function(room) {
            if (!usersInRoom[room]) {
                usersInRoom[room] = {};
                return true;
            }
            return false;
        },

        // add user to both caches
        saveUser: function(user, socketKey, room) {
            if (!usersInRoom[room][user]) {
                usersInRoom[room][user] = socketKey;
                socketsUsers[socketKey] = user;
                return true;
            }
            return false;
        },

        // delete user from both caches
        removeUser: function(user, socketKey, room){
            if (usersInRoom[room] && usersInRoom[room][user] && usersInRoom[room][user] === socketKey) {
                delete usersInRoom[room][user];
                delete socketsUsers[socketKey];
                return true;
            }
            return false;
        },

        // get user & room from socketKey
        getInfoFromSocket: function(socketKey) {
            var user = socketsUsers[socketKey];
            // iterate over cache to find matching room and user
            for (let room in usersInRoom) {
                if (usersInRoom[room] && usersInRoom[room][user]) {
                    if (usersInRoom[room][user] === socketKey) {
                        return {
                            user,
                            room
                        };
                    }
                }
            }
            return false;
        },

        // confirm user-room-socketKey combination
        isUserInRoom: function(user, room, socketKey) {
            return (usersInRoom[room] && usersInRoom[room][user])
        },

        // get user-socket pairings for a room
        getAllUsersInRoom: function(room) {
            if (usersInRoom[room]) {
                // returns user-socket pairs
                return usersInRoom[room]; // => {user_id: socketKey}
            }
            return false;
        }
    }
};
// instantiate the cache methods by invoking its wrapper function
var rucache = usersInRoomsCache();
module.exports = rucache;
