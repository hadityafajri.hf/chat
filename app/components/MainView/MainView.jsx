import React from 'react';
import ChatList from './ChatList/ChatList.jsx';
import RoomInfo from './RoomInfo.jsx';

class MainView extends React.Component {
    constructor(props) {
        super(props);
        this.state = {shouldScroll:false};
    }

    componentWillReceiveProps() {
        let maxScrollValue = this.chatBox.scrollHeight - this.chatBox.clientHeight
        let position = this.chatBox.scrollTop / maxScrollValue

        let shouldScroll = (position === 1 || position === 0)
        this.setState({shouldScroll})
    }

    componentDidUpdate() {
        if (this.state.shouldScroll) {
            this.scrollToBottom()
        }
    }

    scrollToBottom() {
        this.messagesEnd.scrollIntoView({ behaviour: 'smooth'})
    }

    render() {
        let view = null
        if (this.props.messages.length !== 0) {
            view = <ChatList
                       messages={this.props.messages}
                       nicknames = {this.props.nicknames}
                       getNicknameOrAddress = {this.props.getNicknameOrAddress}
                       network={this.props.config.networks.rinkeby} //TODO: set network
            />
        } else {
            view = <RoomInfo
                       addRoomInfo={this.props.addRoomInfo}
                       metadata={this.props.metadata}
                       room={this.props.room}
                       roomContract={this.props.roomContract}
                       config={this.props.config}
            />
        }
        return (
            <div
                style={{
                    height: 'calc(90% - 35px)',
                    border: "solid",
                    backgroundColor: "#D8D8D8",
                    overflow: "auto",
                    padding: '15px'}}
                ref={(el) => { this.chatBox = el; }}>
                {view}
                <div
                    style={{ float:"left", clear: "both" }}
                    ref={(el) => { this.messagesEnd = el; }}>
                </div>
            </div>
        )
    }
}


export default MainView;

