pragma solidity ^0.4.16;

contract Room {
    string public metadata;

    function getPermissions(address) constant public returns(bytes32);
}

contract universalRoom is Room{
    function getPermissions(address) constant public returns (bytes32) {
        return (0x1);
    }
}

contract universalRoomWithDescription is universalRoom {
    function universalRoomWithDescription() public {
        metadata = 'QmYXQL4zvaKKXSCwhYw9CVkAbyfUxiVXmJTDXoQjqf3HkF';
    }
}
